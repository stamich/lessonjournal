## Model layer (an abstracion of data):
 - School represented by:
    - Level (eight levels - osiem klas zakładajac, że każda ma tylko jeden oddział),
    - Activity (typ aktywności),
    - Subject (przedmiot nauczania),
    - Mark (ocena),
    - Disciple (uproszczenie, bo dla dokładności trzebaby uwzględnić w modelu 
      także nauczyciela oraz administratora)
    
## Repository layer:
 - It is possible to implement all the data into one repository, but it is not good.
 - Better solution is to use two separate repositories (**Single Responsibility Principle**):
   - one repository for managing the data of disciple,
   - one repository for managing the data of mark.
 - Repository classes use interfaces to allow different implementations
   which can be easily introduces without changing the existing code (**Open/Closed Principle**).   
   
## Service layer:
 - The service layer is there to provide the logic to operate on the data sent to and from the 
   controller layer. These two pieces can be bundled in together into the same module. It is used
   to serve **loose coupling** in the application.
   
# Controller layer:
 - The controller layer prepares model to pass it to view. In this case we have REST controller
   for the better communication with the presentation layer (frontend application - not implemented here).
   
# Average counting
 - Counting of average is simple when we use Java Streams
 - Counting of weighted average is quite complicated because there are different types:
   - Arithmetic (the most popular),
   - Geometric,
   - Harmonic,
   - Generalized (powered).
 - In this application is used arithmetic weighted average, but is also implemented custom collector
   for counting the geometric one.
   
# Code coverage
 - The code coverage is measured by JaCoCo library.

# Unit testing
 - The unit testing is done with JUnit5 framework and Mockito framework.

# Running application
- The application can be started with any Java IDE or inside the Docker container.
   
