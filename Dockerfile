FROM openjdk:11
ENV GRADLE_VERSION=6.8
ENV APP_HOME=/app/
RUN apt-get update && apt-get install wget -y
RUN wget -q https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip \
    && unzip gradle-${GRADLE_VERSION}-bin.zip -d /opt \
    && rm gradle-${GRADLE_VERSION}-bin.zip
ENV GRADLE_HOME /opt/gradle-${GRADLE_VERSION}
ENV PATH $PATH:/opt/gradle-${GRADLE_VERSION}/bin
RUN gradle -v
EXPOSE 8081
WORKDIR $APP_HOME
COPY . $APP_HOME