package com.heidelberg.lessonjournal.controller;

import com.heidelberg.lessonjournal.exception.MarkException;
import com.heidelberg.lessonjournal.exception.PersonNotFoundException;
import com.heidelberg.lessonjournal.model.Disciple;
import com.heidelberg.lessonjournal.model.Mark;
import com.heidelberg.lessonjournal.model.Subject;
import com.heidelberg.lessonjournal.service.DiscipleServiceImpl;
import com.heidelberg.lessonjournal.service.JournalServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(path = "/journal")
public class JournalController {

    private final JournalServiceImpl journalService;
    private final DiscipleServiceImpl discipleService;


    @Autowired
    public JournalController(JournalServiceImpl journalService, DiscipleServiceImpl discipleService) {
        this.journalService = journalService;
        this.discipleService = discipleService;
    }

    @GetMapping("/allMarks/{personId}")
    public ResponseEntity<List<Mark>> getAllMarksByDiscipleId(@PathVariable("personId") Integer discipleId) {
        List<Mark> marks = journalService.findAllMarksByDiscipleId(discipleId);
        if (discipleId == null) {
            return new ResponseEntity<>(marks, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(marks, HttpStatus.OK);
    }

    @GetMapping("/allMarksBySubject/{subject}/{discipleId}")
    public ResponseEntity<List<Mark>> getMarksBySubject(@PathVariable("subject") Subject subject, @PathVariable("discipleId") Integer discipleId) {
        List<Mark> marks = journalService.findMarksBySubjectAndDiscipleId(subject, discipleId);
        if (discipleId == null || subject == null) {
            return new ResponseEntity<>(marks, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(marks, HttpStatus.OK);
    }

    @GetMapping("/arithmeticAverage/{subject}/{discipleId}")
    public ResponseEntity<Double> getArithmeticAverage(@PathVariable("subject") Subject subject, @PathVariable("discipleId") Integer discipleId) {
        Double average = journalService.countWeightedArithmeticAverageFromOneSubject(subject, discipleId);
        return new ResponseEntity<>(average, HttpStatus.OK);
    }

    @GetMapping("/geometricAverage/{subject}/{discipleId}")
    public ResponseEntity<Double> getGeometricAverage(@PathVariable("subject") Subject subject, @PathVariable("discipleId") Integer discipleId) {
        Double average = journalService.countWeightedGeometricAverageFromOneSubject(subject, discipleId);
        return new ResponseEntity<>(average, HttpStatus.OK);
    }

    @PostMapping("/createMark/{discipleId}")
    public ResponseEntity<Void> createMark(@PathVariable("discipleId") Integer discipleId, @RequestBody Mark mark) throws PersonNotFoundException {
        if (!discipleService.isDiscipleExists(discipleId)) {
            Disciple disciple = new Disciple();
            disciple.setId(discipleId);
            createDisciple(disciple);
        }
        journalService.addMark(mark);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/createDisciple")
    public ResponseEntity<Void> createDisciple(@RequestBody Disciple disciple) throws PersonNotFoundException {
        if (discipleService.isDiscipleExists(disciple.getId())) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        discipleService.addDisciple(disciple);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping("/deleteMark/{markId}")
    public ResponseEntity<Void> deleteMark(@PathVariable("markId") Integer markId) throws MarkException {
        journalService.deleteMarkById(markId);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
