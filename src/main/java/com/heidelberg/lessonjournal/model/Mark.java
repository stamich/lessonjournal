package com.heidelberg.lessonjournal.model;

import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Mark model
 */
@NoArgsConstructor
public class Mark implements Comparable<Mark>, Serializable {

    private Integer id;
    private Double mark;
    private Integer weight;
    private Subject subject;
    private Activity activity;
    private Integer discipleId;

    public Mark(Integer id, Double mark, Integer weight, Subject subject, Activity activity, Integer discipleId) {
        this.id = id;
        this.mark = mark;
        this.weight = weight;
        this.subject = subject;
        this.activity = activity;
        this.discipleId = discipleId;
    }

    @Override
    public int compareTo(Mark o) {
        return 0;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getMark() {
        return mark;
    }

    public void setMark(Double mark) {
        this.mark = mark;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public Integer getDiscipleId() {
        return discipleId;
    }

    public void setDiscipleId(Integer discipleId) {
        this.discipleId = discipleId;
    }
}
