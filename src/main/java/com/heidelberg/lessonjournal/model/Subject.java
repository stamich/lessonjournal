package com.heidelberg.lessonjournal.model;

/**
 * Types of lesson subject
 */
public enum Subject {

    POLISH("j. polski"),
    PHYSICS("fizyka"),
    ENGLISH("j. angielski"),
    MATHEMATICS("matematyka");

    private String subject;

    Subject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return this.subject;
    }
}


