package com.heidelberg.lessonjournal.model;

/**
 * Types of activity
 */
public enum Activity {
    EXAM("egzamin"),
    HOMEWORK("zad. domowe"),
    TEST("test");

    private String activity;

    Activity(String activity) {
        this.activity = activity;
    }

    public String getActivity() {
        return this.activity;
    }
}
