package com.heidelberg.lessonjournal.model;

/**
 * School levels
 */
public enum Level {
    ONE("klasa pierwsza"),
    TWO("klasa druga"),
    THREE("klasa trzecia"),
    FOUR("klasa czwarta"),
    FIVE("klasa piąta"),
    SIX("klasa szósta"),
    SEVEN("klasa siódma"),
    EIGHT("klasa ósma");

    private String level;

    Level(String level) {
        this.level = level;
    }

    public String getLevel() {
        return this.level;
    }
}
