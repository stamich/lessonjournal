package com.heidelberg.lessonjournal.repository;

import com.heidelberg.lessonjournal.exception.MarkException;
import com.heidelberg.lessonjournal.exception.PersonNotFoundException;
import com.heidelberg.lessonjournal.model.*;

import java.util.List;

/**
 * Interface for handling lesson journal data.
 * @author Michal Stawarski
 */
public interface JournalRepository {

    List<Mark> getAllMarks();
    Mark getOneMarkById(Integer id);
    Mark getOneMarkByDiscipleId(Integer userId, Integer markId) throws MarkException;
    List<Mark> findAllMarksByDiscipleId(Integer discipleId);
    List<Mark> findMarksBySubjectAndDiscipleId(Subject subject, Integer discipleId);
    List<Mark> findMarksByActivityAndDiscipleId(Activity activity, Integer discipleId);
    void addMark(Mark mark) throws PersonNotFoundException;
    void deleteMark(Integer markId) throws MarkException;
}
