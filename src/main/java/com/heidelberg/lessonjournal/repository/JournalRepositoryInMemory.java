package com.heidelberg.lessonjournal.repository;

import com.heidelberg.lessonjournal.exception.MarkException;
import com.heidelberg.lessonjournal.model.*;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Lesson journal repository containing the data of all marks.
 * @author Michal Stawarski
 */
@Repository("journalRepository")
public class JournalRepositoryInMemory implements JournalRepository {

    private final List<Mark> journal;

    public JournalRepositoryInMemory() {

        this.journal = new ArrayList<>();
        Mark m0 = new Mark(1, 5.0, 1, Subject.ENGLISH, Activity.HOMEWORK, 1);
        Mark m1 = new Mark(2, 4.0, 1, Subject.ENGLISH, Activity.HOMEWORK, 1);
        Mark m2 = new Mark(3, 4.5, 1, Subject.POLISH, Activity.HOMEWORK, 1);
        Mark m3 = new Mark(4, 4.0, 1, Subject.MATHEMATICS, Activity.EXAM, 2);
        Mark m4 = new Mark(5, 4.5, 1, Subject.ENGLISH, Activity.TEST, 2);
        Collections.addAll(journal, m0, m1, m2, m3, m4);
    }

    public List<Mark> getAllMarks() {
        return journal;
    }

    public Mark getOneMarkById(Integer id) {
        return journal.stream()
                .filter(mark -> mark.getId().equals(id))
                .findAny()
                .orElse(null);
    }

    public Mark getOneMarkByDiscipleId(Integer userId, Integer markId) {
        return journal.stream()
                .filter(m -> m.getDiscipleId().equals(userId))
                .filter(mark -> mark.getId().equals(markId))
                .findAny()
                .orElse(null);
    }

    public List<Mark> findAllMarksByDiscipleId(Integer discipleId) {
        return Optional.ofNullable(journal).orElseGet(Collections::emptyList)
                .stream()
                .filter(Objects::nonNull)
                .filter(d -> d.getDiscipleId().equals(discipleId))
                .sorted()
                .collect(Collectors.toList());
    }

    public List<Mark> findMarksBySubjectAndDiscipleId(Subject subject, Integer discipleId) {
        return Optional.ofNullable(journal).orElseGet(Collections::emptyList)
                .stream()
                .filter(Objects::nonNull)
                .filter(m -> m.getSubject() == subject)
                .filter(d -> d.getDiscipleId().equals(discipleId))
                .sorted()
                .collect(Collectors.toList());
    }

    public List<Mark> findMarksByActivityAndDiscipleId(Activity activity, Integer discipleId) {
        return Optional.ofNullable(journal).orElseGet(Collections::emptyList)
                .stream()
                .filter(Objects::nonNull)
                .filter(a -> a.getActivity() == activity)
                .filter(d -> d.getDiscipleId().equals(discipleId))
                .sorted()
                .collect(Collectors.toList());
    }

    public void addMark(Mark mark) {
        journal.add(mark);
    }

    public void deleteMark(Integer markId) throws MarkException {
        var res = getOneMarkById(markId);
        journal.remove(res);
    }
}
