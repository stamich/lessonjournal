package com.heidelberg.lessonjournal.repository;

import com.heidelberg.lessonjournal.exception.PersonNotFoundException;
import com.heidelberg.lessonjournal.model.Disciple;
import com.heidelberg.lessonjournal.model.Level;
import com.heidelberg.lessonjournal.model.Subject;

import java.util.List;

/**
 * Interface for handling disciple data
 * @author Michal Stawarski
 */
public interface DisciplesRepository {

    Disciple getOneDiscipleById(Integer id) throws PersonNotFoundException;
    List<Disciple> listAllDisciples();
    List<Disciple> listAllDisciplesByLevel(Level level);
    List<Disciple> listAllDisciplesBySubject(Subject subject);
    void addDisciple(Disciple disciple);
    void deleteDiscipleById(Integer id) throws PersonNotFoundException;
}
