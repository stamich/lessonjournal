package com.heidelberg.lessonjournal.repository;

import com.heidelberg.lessonjournal.exception.PersonNotFoundException;
import com.heidelberg.lessonjournal.model.Disciple;
import com.heidelberg.lessonjournal.model.Level;
import com.heidelberg.lessonjournal.model.Subject;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Repository class for disciple model
 * @author Michal Stawarski
 */
@Repository("disciplesRepository")
public class DisciplesRepositoryInMemory  implements  DisciplesRepository {

    private final List<Disciple> school;

    /**
     * It is not implemented with Arrays.asList() because this not supports
     * any structural modifications e.g. .add() method.
     */
    public DisciplesRepositoryInMemory() {
        this.school = new ArrayList<>();
        Disciple d0 = new Disciple(1, "Adam", "Kowalski", Level.FIVE, List.of(Subject.ENGLISH, Subject.MATHEMATICS));
        Disciple d1 = new Disciple(2, "Ewa", "Nowak", Level.FOUR, List.of(Subject.POLISH, Subject.MATHEMATICS));
        Disciple d2 = new Disciple(3, "Abraham", "Lincoln", Level.EIGHT, List.of(Subject.PHYSICS, Subject.MATHEMATICS));
        Collections.addAll(school, d0, d1, d2);
    }

    /**
     * Fetches one disciple.
     * @param id discipleId
     * @return Disciple object
     * @throws PersonNotFoundException extending Exception
     */
    public Disciple getOneDiscipleById(Integer id) throws PersonNotFoundException {
        if (school.contains(school.get(id))) {
            return school.stream()
                    .filter(d -> d.getId().equals(id))
                    .findAny()
                    .orElse(null);
        }
        throw new PersonNotFoundException(id);
    }

    /**
     * Lists all disciples in a school.
     * @return
     */
    public List<Disciple> listAllDisciples() {
        return school;
    }

    /**
     * List all disciples by level
     * @param level represented as enum
     * @return list of disciples
     */
    public List<Disciple> listAllDisciplesByLevel(Level level) {
        return Optional.ofNullable(school).orElseGet(Collections::emptyList)
                .stream()
                .filter(Objects::nonNull)
                .filter(d -> d.getLevel() == level)
                .sorted()
                .collect(Collectors.toList());
    }

    /**
     * Lists all the disciples in a given subject.
     * @param subject
     * @return
     */
    public List<Disciple> listAllDisciplesBySubject(Subject subject) {
        return Optional.ofNullable(school).orElseGet(Collections::emptyList)
                .stream()
                .filter(Objects::nonNull)
                .filter(d -> d.getSubject().contains(subject))
                .sorted()
                .collect(Collectors.toList());
    }

    /**
     * Adds a new disciple.
     * @param disciple as object
     */
    public void addDisciple(Disciple disciple) {
        school.add(disciple);
    }

    /**
     * Deletes current disciple.
     * @param id id number of disciple.
     * @throws PersonNotFoundException extending Exception class.
     */
    public void deleteDiscipleById(Integer id) throws PersonNotFoundException {
        var result = getOneDiscipleById(id);
        school.remove(result);
    }
}
