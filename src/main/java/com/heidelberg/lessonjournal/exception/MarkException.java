package com.heidelberg.lessonjournal.exception;

public class MarkException extends Exception {

    public MarkException(int id) {
        super("Mark with id: " + id + " does not exist.");
    }
}
