package com.heidelberg.lessonjournal.exception;

public class PersonNotFoundException extends Exception {

    public PersonNotFoundException(Integer id) {
        super("Person with id: " + id + " does not exist");
    }
    public PersonNotFoundException(Integer id, String firstName, String lastName) {
        super("Person" + firstName + " " + lastName + " with id: " + id + "does not exist");
    }
}
