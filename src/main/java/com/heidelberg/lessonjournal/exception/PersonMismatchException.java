package com.heidelberg.lessonjournal.exception;

public class PersonMismatchException extends Exception {

    public PersonMismatchException(Integer id) {
        super("Person with id: " + id + " is ambiguous.");
    }
}
