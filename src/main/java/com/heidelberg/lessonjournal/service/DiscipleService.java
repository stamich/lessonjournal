package com.heidelberg.lessonjournal.service;

import com.heidelberg.lessonjournal.exception.PersonNotFoundException;
import com.heidelberg.lessonjournal.model.Disciple;
import com.heidelberg.lessonjournal.model.Level;
import com.heidelberg.lessonjournal.model.Subject;

import java.util.List;

public interface DiscipleService {

    Disciple getOneDiscipleById(Integer id) throws PersonNotFoundException;
    List<Disciple> listAllDisciples();
    List<Disciple> listAllDisciplesByLevel(Level level);
    List<Disciple> listAllDisciplesBySubject(Subject subject);
    void addDisciple(Disciple disciple);
    void updateDisciple(Disciple disciple) throws PersonNotFoundException;
    void deleteDiscipleById(Integer id) throws PersonNotFoundException;
    boolean isDiscipleExists(Integer discipleId) throws PersonNotFoundException;
}
