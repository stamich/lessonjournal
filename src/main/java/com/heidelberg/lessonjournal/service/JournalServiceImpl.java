package com.heidelberg.lessonjournal.service;

import com.heidelberg.lessonjournal.exception.MarkException;
import com.heidelberg.lessonjournal.exception.PersonNotFoundException;
import com.heidelberg.lessonjournal.model.*;
import com.heidelberg.lessonjournal.repository.DisciplesRepository;
import com.heidelberg.lessonjournal.repository.JournalRepositoryInMemory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("journalService")
public class JournalServiceImpl implements JournalService {

    private final DisciplesRepository disciplesRepository;
    private final JournalRepositoryInMemory journalRepository;

    @Autowired
    public JournalServiceImpl(DisciplesRepository disciplesRepository, JournalRepositoryInMemory journalRepository) {
        this.disciplesRepository = disciplesRepository;
        this.journalRepository = journalRepository;
    }

    public List<Mark> getAllMarks() {
        return journalRepository.getAllMarks();
    }

    public Mark getONeMarkById(Integer id) {
        return journalRepository.getOneMarkById(id);
    }

    public Mark getOneMarkByDiscipleId(Integer discipleId, Integer markId) throws MarkException {
        return journalRepository.getOneMarkByDiscipleId(discipleId, markId);
    }

    public List<Mark> findAllMarksByDiscipleId(Integer id) {
        return journalRepository.findAllMarksByDiscipleId(id);
    }

    public List<Mark> findMarksBySubjectAndDiscipleId(Subject subject, Integer discipleId) {
        return journalRepository.findMarksBySubjectAndDiscipleId(subject, discipleId);
    }

    public List<Mark> findMarksByDiscipleAndActivity(Activity activity, Integer discipleId) {
        return journalRepository.findMarksByActivityAndDiscipleId(activity, discipleId);
    }

    public void addMark(Mark mark) {
        Mark m = new Mark();
        m.setId(mark.getId());
        m.setMark(mark.getMark());
        m.setWeight(mark.getWeight());
        m.setActivity(mark.getActivity());
        m.setSubject(mark.getSubject());
        m.setDiscipleId(mark.getDiscipleId());
        journalRepository.addMark(m);
    }

    public void updateMark(Mark mark) {
        Mark m = journalRepository.getOneMarkById(mark.getId());
        if (m != null) {
            m.setId(mark.getId());
            m.setMark(mark.getMark());
            m.setWeight(mark.getWeight());
            m.setActivity(mark.getActivity());
            m.setSubject(mark.getSubject());
            m.setDiscipleId(mark.getDiscipleId());
        }
    }

    public void deleteMarkById(Integer markId) throws MarkException {
        journalRepository.deleteMark(markId);
    }

    public Double countAverageFromAllSubjects(Integer discipleId) {
        return journalRepository.findAllMarksByDiscipleId(discipleId).stream()
                .mapToDouble(Mark::getMark)
                .filter(l -> l > 0)
                .average()
                .orElseThrow(IllegalStateException::new);
    }

    public Double countAverageFromOneSubject(Subject subject, Integer discipleId) {
        return journalRepository.findMarksBySubjectAndDiscipleId(subject, discipleId).stream()
                .mapToDouble(Mark::getMark)
                .filter(l -> l> 0)
                .average()
                .orElseThrow(IllegalStateException::new);
    }

    public Double countWeightedArithmeticAverageFromOneSubject(Subject subject, Integer discipleId) {
        var average = countAverageFromOneSubject(subject, discipleId);
        var weights = journalRepository.findMarksBySubjectAndDiscipleId(subject, discipleId).stream()
                .mapToInt(Mark::getWeight)
                .sum();
        return average / weights;
    }

    public Double countWeightedGeometricAverageFromOneSubject(Subject subject, Integer discipleId) {
        var average = countAverageFromOneSubject(subject, discipleId);
        var weights = journalRepository.findMarksBySubjectAndDiscipleId(subject, discipleId).stream()
                .mapToInt(Mark::getWeight)
                .sum();
        return Math.exp(average / Math.log(weights));
    }
}
