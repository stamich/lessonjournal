package com.heidelberg.lessonjournal.service;

import com.heidelberg.lessonjournal.exception.PersonNotFoundException;
import com.heidelberg.lessonjournal.model.Disciple;
import com.heidelberg.lessonjournal.model.Level;
import com.heidelberg.lessonjournal.model.Subject;
import com.heidelberg.lessonjournal.repository.DisciplesRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("discipleService")
public class DiscipleServiceImpl implements DiscipleService {

    private final DisciplesRepository disciplesRepository;

    public DiscipleServiceImpl(DisciplesRepository disciplesRepository) {
        this.disciplesRepository = disciplesRepository;
    }

    public Disciple getOneDiscipleById(Integer id) throws PersonNotFoundException {
        return disciplesRepository.getOneDiscipleById(id);
    }

    public List<Disciple> listAllDisciples() {
        return disciplesRepository.listAllDisciples();
    }

    public List<Disciple> listAllDisciplesByLevel(Level level) {
        return disciplesRepository.listAllDisciplesByLevel(level);
    }

    public List<Disciple> listAllDisciplesBySubject(Subject subject) {
        return disciplesRepository.listAllDisciplesBySubject(subject);
    }

    public void addDisciple(Disciple disciple) {
        disciplesRepository.addDisciple(disciple);
    }

    public void updateDisciple(Disciple disciple) throws PersonNotFoundException {
        Disciple d = disciplesRepository.getOneDiscipleById(disciple.getId());
        if (d != null) {
            d.setId(disciple.getId());
            d.setFirstName(disciple.getFirstName());
            d.setLastName(disciple.getLastName());
            d.setLevel(disciple.getLevel());
            d.setSubject(disciple.getSubject());
        }
    }

    public void deleteDiscipleById(Integer id) throws PersonNotFoundException {
        disciplesRepository.deleteDiscipleById(id);
    }

    @Override
    public boolean isDiscipleExists(Integer discipleId) throws PersonNotFoundException {
        return disciplesRepository.getOneDiscipleById(discipleId) != null;
    }
}
