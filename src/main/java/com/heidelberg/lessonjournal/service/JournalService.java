package com.heidelberg.lessonjournal.service;

import com.heidelberg.lessonjournal.exception.MarkException;
import com.heidelberg.lessonjournal.exception.PersonNotFoundException;
import com.heidelberg.lessonjournal.model.*;

import java.util.List;

public interface JournalService {

    List<Mark> getAllMarks();
    Mark getONeMarkById(Integer id);
    Mark getOneMarkByDiscipleId(Integer discipleId, Integer markId) throws MarkException;
    List<Mark> findAllMarksByDiscipleId(Integer id);
    List<Mark> findMarksBySubjectAndDiscipleId(Subject subject, Integer discipleId);
    List<Mark> findMarksByDiscipleAndActivity(Activity activity, Integer discipleId);
    void addMark(Mark mark) throws PersonNotFoundException;
    void updateMark(Mark mark);
    void deleteMarkById(Integer markId) throws MarkException;

    Double countAverageFromAllSubjects(Integer discipleId);
    Double countAverageFromOneSubject(Subject subject, Integer discipleId);
    Double countWeightedArithmeticAverageFromOneSubject(Subject subject, Integer discipleId);
    Double countWeightedGeometricAverageFromOneSubject(Subject subject, Integer discipleId);
}
