package com.heidelberg.lessonjournal;

import com.heidelberg.lessonjournal.model.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class LessonjournalApplication {

    public static void main(String[] args) {

        SpringApplication.run(LessonjournalApplication.class, args);
    }

}
