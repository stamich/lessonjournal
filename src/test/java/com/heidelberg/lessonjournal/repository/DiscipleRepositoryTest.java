package com.heidelberg.lessonjournal.repository;

import com.heidelberg.lessonjournal.exception.PersonNotFoundException;
import com.heidelberg.lessonjournal.model.Disciple;
import com.heidelberg.lessonjournal.model.Level;
import com.heidelberg.lessonjournal.model.Subject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class DiscipleRepositoryTest {

    @Autowired
    private DisciplesRepositoryInMemory dr;

    @Test
    public void listAllDisciplesTest() {
        assertNotNull(dr.listAllDisciples());
        assertEquals(dr.listAllDisciples().size(), 3);
    }

    @Test
    public void listAllDisciplesByLevelTest() {
        assertTrue(dr.listAllDisciplesByLevel(Level.THREE).isEmpty());
        assertNotNull(dr.listAllDisciplesByLevel(Level.FOUR));
        assertEquals(dr.listAllDisciplesByLevel(Level.FOUR).size(), 1);
    }

    @Test
    public void listAllDisciplesBySubject() {
        assertNotNull(dr.listAllDisciplesBySubject(Subject.MATHEMATICS));
    }

    @Test
    public void addDiscipleTest() {
        Disciple d = new Disciple(8, "Albert", "Einstein", Level.EIGHT, List.of(Subject.PHYSICS, Subject.MATHEMATICS));
        dr.addDisciple(d);
        assertEquals(dr.listAllDisciples().size(), 4);
    }

    @Test
    public void deleteDiscipleTest() throws PersonNotFoundException {
        dr.deleteDiscipleById(1);
        assertEquals(dr.listAllDisciples().size(), 3);
    }
}
