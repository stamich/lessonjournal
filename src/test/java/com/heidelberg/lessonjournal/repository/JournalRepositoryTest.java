package com.heidelberg.lessonjournal.repository;

import com.heidelberg.lessonjournal.exception.MarkException;
import com.heidelberg.lessonjournal.exception.PersonNotFoundException;
import com.heidelberg.lessonjournal.model.Activity;
import com.heidelberg.lessonjournal.model.Mark;
import com.heidelberg.lessonjournal.model.Subject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class JournalRepositoryTest {

    @Autowired
    private JournalRepositoryInMemory jr;

    @Autowired
    private DisciplesRepositoryInMemory dr;

    @Test
    public void getAllMarksTest() {
        assertNotNull(jr.getAllMarks());
        assertEquals(jr.getAllMarks().size(), 5);
    }

    @Test
    public void findAllMarksByDiscipleIdTest() throws PersonNotFoundException {
        assertNotNull(jr.findAllMarksByDiscipleId(1));
        assertEquals(jr.findAllMarksByDiscipleId(1).size(), 2);
    }

    @Test
    public void findMarksBySubjectAndDiscipleIdTest() throws PersonNotFoundException {
        var d = dr.listAllDisciples().get(2);
        assertNotNull(jr.findMarksBySubjectAndDiscipleId(Subject.ENGLISH, d.getId()));
        assertEquals(jr.findMarksBySubjectAndDiscipleId(Subject.ENGLISH, d.getId()).size(), 0);
    }

    @Test
    public void findMarksByActivityAndDiscipleIdTest() throws PersonNotFoundException {
        assertNotNull(jr.findMarksByActivityAndDiscipleId(Activity.TEST, 1));
        assertEquals(jr.findMarksByActivityAndDiscipleId(Activity.TEST, 1).size(), 0);
    }

    @Test
    public void addMarkTest() {
        Mark m = new Mark(5, 4.0, 1, Subject.PHYSICS, Activity.TEST, 3);
        jr.addMark(m);
        assertEquals(jr.getAllMarks().size(), 5);
    }

    @Test
    public void deleteMarkTest() throws MarkException {
        jr.deleteMark( 1);
        assertEquals(jr.getAllMarks().size(), 4);
    }
}
