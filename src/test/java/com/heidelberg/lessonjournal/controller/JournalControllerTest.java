package com.heidelberg.lessonjournal.controller;

import com.heidelberg.lessonjournal.exception.MarkException;
import com.heidelberg.lessonjournal.exception.PersonNotFoundException;
import com.heidelberg.lessonjournal.model.*;
import com.heidelberg.lessonjournal.service.DiscipleServiceImpl;
import com.heidelberg.lessonjournal.service.JournalService;
import com.heidelberg.lessonjournal.service.JournalServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class JournalControllerTest {

    @Mock
    private DiscipleServiceImpl discipleService;

    @Mock
    private JournalServiceImpl journalService;

    @InjectMocks
    private JournalController journalController;

    @Spy
    private List<Mark> marks = new ArrayList<>();

    private AutoCloseable autoCloseable;

    public List<Mark> getMarks() {
        Mark m0 = new Mark(10, 5.0, 100, Subject.ENGLISH, Activity.TEST, 1);
        Mark m1 = new Mark(11, 4.0, 100, Subject.MATHEMATICS, Activity.HOMEWORK,2);
        Mark m2 = new Mark(12, 4.5, 200, Subject.MATHEMATICS, Activity.EXAM, 2);
        Collections.addAll(marks, m0, m1, m2);
        return marks;
    }

    @BeforeEach
    public void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        marks = getMarks();
    }

    @Test
    public void getAllMarksByDiscipleIdTest() {
        var id = marks.get(0).getDiscipleId();

        when(journalService.findAllMarksByDiscipleId(id)).thenReturn(marks);
        journalService.findAllMarksByDiscipleId(id);
        verify(journalService, atLeastOnce()).findAllMarksByDiscipleId(id);

        var result = journalController.getAllMarksByDiscipleId(id);
        assertEquals(result.getStatusCodeValue(), 200);
    }

    @Test
    public void getMarksBySubjectTest() {
        var id = marks.get(0).getDiscipleId();

        when(journalService.findMarksBySubjectAndDiscipleId(Subject.ENGLISH, id)).thenReturn(marks);
        journalService.findMarksBySubjectAndDiscipleId(Subject.ENGLISH, id);
        verify(journalService, atLeastOnce()).findMarksBySubjectAndDiscipleId(Subject.ENGLISH, id);

        var result = journalController.getMarksBySubject(Subject.ENGLISH, id);
        assertEquals(result.getStatusCodeValue(), 200);
    }

    @Test
    public void getArithmeticAverageTest() {
        var id = marks.get(1).getDiscipleId();

        when(journalService.countWeightedArithmeticAverageFromOneSubject(Subject.MATHEMATICS, 2)).thenReturn(4.5);
        journalService.countWeightedArithmeticAverageFromOneSubject(Subject.MATHEMATICS, 2);
        verify(journalService, atLeastOnce()).countWeightedArithmeticAverageFromOneSubject(Subject.MATHEMATICS, 2);

        var result = journalController.getArithmeticAverage(Subject.MATHEMATICS, 2);
        assertEquals(result.getStatusCodeValue(), 200);
    }

    @Test
    public void getGeometricAverageTest() {
        when(journalService.countWeightedGeometricAverageFromOneSubject(Subject.MATHEMATICS, 2)).thenReturn(4.5);
        journalService.countWeightedGeometricAverageFromOneSubject(Subject.MATHEMATICS, 2);
        verify(journalService, atLeastOnce()).countWeightedGeometricAverageFromOneSubject(Subject.MATHEMATICS, 2);

        var result = journalController.getGeometricAverage(Subject.MATHEMATICS, 2);
        assertEquals(result.getStatusCodeValue(), 200);
    }

    @Test
    public void createMarkTest() throws PersonNotFoundException {
        Mark m = new Mark(20, 4.5, 100, Subject.PHYSICS, Activity.HOMEWORK, 2);
        var result = journalController.createMark(2, m);
        assertEquals(result.getStatusCodeValue(), 201);
    }

    @Test
    public void createDiscipleTest() throws PersonNotFoundException {
        Disciple d = new Disciple(100, "Nicola", "Tesla", Level.EIGHT, List.of(Subject.PHYSICS));
        var result = journalController.createDisciple(d);
        assertEquals(result.getStatusCodeValue(), 201);
    }

    @Test
    public void deleteMarkTest() throws MarkException {
        doNothing().when(journalService).deleteMarkById(12);
        assertEquals(journalService.getAllMarks().size(), 0);
        assertEquals(journalController.getAllMarksByDiscipleId(2).getStatusCodeValue(), 200);
    }

    @AfterEach
    public void tearDown() throws Exception {
        autoCloseable.close();
    }
}
