package com.heidelberg.lessonjournal.service;

import com.heidelberg.lessonjournal.exception.MarkException;
import com.heidelberg.lessonjournal.exception.PersonNotFoundException;
import com.heidelberg.lessonjournal.model.Subject;
import com.heidelberg.lessonjournal.repository.DisciplesRepositoryInMemory;
import com.heidelberg.lessonjournal.repository.JournalRepositoryInMemory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class JournalServiceTest {

    @Autowired
    private JournalServiceImpl journalService;

    @Test
    public void countAverageFromAllSubjectsTest() {
        assertNotNull(journalService.countAverageFromAllSubjects(1));
        System.out.println(journalService.countAverageFromAllSubjects(1));
    }

    @Test
    public void countAverageFromOneSubjectTest() {
        assertNotNull(journalService.countAverageFromOneSubject(Subject.ENGLISH, 1));
        System.out.println(journalService.countAverageFromOneSubject(Subject.ENGLISH, 1));
    }

    @Test
    public void countWeightedArithmeticAverageFromOneSubjectTest() {
        assertNotNull(journalService.countWeightedArithmeticAverageFromOneSubject(Subject.ENGLISH, 1));
        System.out.println(journalService.countWeightedArithmeticAverageFromOneSubject(Subject.ENGLISH, 1));
    }

    @Test
    public void countWeightedGeometricAverageFromOneSubject() {
        assertNotNull(journalService.countWeightedGeometricAverageFromOneSubject(Subject.ENGLISH, 1));
        System.out.println(journalService.countWeightedGeometricAverageFromOneSubject(Subject.ENGLISH, 1));
    }
}
